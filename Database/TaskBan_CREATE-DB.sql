-- Creator:       MySQL Workbench 6.3.9/ExportSQLite Plugin 0.1.0
-- Author:        mhenke
-- Caption:       New Model
-- Project:       Name of the project
-- Changed:       2017-03-31 23:09
-- Created:       2017-03-31 17:58
PRAGMA foreign_keys = OFF;

-- Schema: TaskBan
ATTACH "TaskBan.sdb" AS "TaskBan";
BEGIN;
CREATE TABLE "categoriatarefa"(
  "idcategoriatarefa" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL CHECK("idcategoriatarefa">=0),
  "categoriatarefanome" VARCHAR(100)
);
INSERT INTO "categoriatarefa"("idcategoriatarefa","categoriatarefanome") VALUES(1, 'ADMINISTRATIVA');
INSERT INTO "categoriatarefa"("idcategoriatarefa","categoriatarefanome") VALUES(2, 'PROJETO');
INSERT INTO "categoriatarefa"("idcategoriatarefa","categoriatarefanome") VALUES(3, 'APOIO');
CREATE TABLE "contato"(
  "idcontato" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL CHECK("idcontato">=0),
  "contatonome" VARCHAR(255) NOT NULL
);
CREATE INDEX "contato.contatonome_IDX" ON "contato" ("contatonome");
INSERT INTO "contato"("idcontato","contatonome") VALUES(1, 'MARCOS');
INSERT INTO "contato"("idcontato","contatonome") VALUES(2, 'DERP');
INSERT INTO "contato"("idcontato","contatonome") VALUES(3, 'HERP');
INSERT INTO "contato"("idcontato","contatonome") VALUES(4, 'DERPINA');
CREATE TABLE "tarefa"(
  "idtarefa" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL CHECK("idtarefa">=0),
  "tarefastatus" CHAR(1),
  "tarefanome" VARCHAR(255) NOT NULL,
  "tarefadesc" BLOB,
  "tarefadatacriacao" DATETIME NOT NULL,
  "tarefatempoestimado" FLOAT,
  "tarefatempodedicado" FLOAT,
  "tarefatemporestante" FLOAT,
  "tarefadatavencimento" DATETIME,
  "tarefadatatermino" DATETIME,
  "idcategoriatarefa" INTEGER NOT NULL CHECK("idcategoriatarefa">=0),
  "idcontato_responsavel" INTEGER NOT NULL CHECK("idcontato_responsavel">=0),
  "idcontato_pessoa" INTEGER NOT NULL CHECK("idcontato_pessoa">=0),
  CONSTRAINT "idtarefa_UNIQUE"
    UNIQUE("idtarefa"),
  CONSTRAINT "fk_tarefa_categoriatarefa1"
    FOREIGN KEY("idcategoriatarefa")
    REFERENCES "categoriatarefa"("idcategoriatarefa"),
  CONSTRAINT "fk_tarefa_contato1"
    FOREIGN KEY("idcontato_responsavel")
    REFERENCES "contato"("idcontato"),
  CONSTRAINT "fk_tarefa_contato2"
    FOREIGN KEY("idcontato_pessoa")
    REFERENCES "contato"("idcontato")
);
CREATE INDEX "tarefa.tarefanome_IDX" ON "tarefa" ("tarefanome");
CREATE INDEX "tarefa.tarefadatacriacao_IDX" ON "tarefa" ("tarefadatacriacao");
CREATE INDEX "tarefa.tarefadatatermino_IDX" ON "tarefa" ("tarefadatatermino");
CREATE INDEX "tarefa.fk_tarefa_categoriatarefa1_idx" ON "tarefa" ("idcategoriatarefa");
CREATE INDEX "tarefa.fk_tarefa_contato1_idx" ON "tarefa" ("idcontato_responsavel");
CREATE INDEX "tarefa.fk_tarefa_contato2_idx" ON "tarefa" ("idcontato_pessoa");
CREATE TABLE "tarefahistorico"(
  "idtarefa" INTEGER CHECK("idtarefa">=0),
  "idcontato" INTEGER CHECK("idcontato">=0),
  "tarefahistoricodataalteracao" DATETIME NOT NULL,
  "tarefahistoricocampoalterado" VARCHAR(100) NOT NULL,
  "tarefahistoricovalorantigo" VARCHAR(255) NOT NULL,
  "tarefahistoricovalornovo" VARCHAR(255) NOT NULL,
  "tarefahistoricotipo" CHAR(1) NOT NULL DEFAULT 'H',
--   H=Histórico de alteração
--   C=Comentario
  CONSTRAINT "fk_tarefahistorico_tarefa"
    FOREIGN KEY("idtarefa")
    REFERENCES "tarefa"("idtarefa"),
  CONSTRAINT "fk_tarefahistorico_contato1"
    FOREIGN KEY("idcontato")
    REFERENCES "contato"("idcontato")
);
CREATE INDEX "tarefahistorico.fk_tarefahistorico_tarefa_idx" ON "tarefahistorico" ("idtarefa");
CREATE INDEX "tarefahistorico.fk_tarefahistorico_contato1_idx" ON "tarefahistorico" ("idcontato");
COMMIT;
