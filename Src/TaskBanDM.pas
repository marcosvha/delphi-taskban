{-----------------------------------------------------------------------------
 Unit Name: TaskBanDM
 Author:    Marcos VHA
 Date:      2017
 Purpose:   DataModule com m�todos para conectar e/ou criar ao BD.
            DataSets compartilhados entre forms devem ser inclu�dos aqui.
-----------------------------------------------------------------------------}
unit TaskBanDM;

interface

uses
  System.SysUtils, System.Classes, ZAbstractConnection, ZConnection,
  ZSqlProcessor, Data.DB, ZAbstractRODataset, ZAbstractDataset, ZDataset;

const
  C_DBLIB_PATH = 'sqlite3.dll';
  C_ARQ_DB = '\Database\TaskBanDB.sqlite';
  C_SCRIPT_CRIA_DB = '\Database\TaskBan_CREATE-DB.sql';

type
  TdmTaskBan = class(TDataModule)
    conTaskBan: TZConnection;
    sqlProcCreateDB: TZSQLProcessor;
    qryContato: TZQuery;
    dtsContato: TDataSource;
    qryCategoriaTarefa: TZQuery;
    dtsCategoriaTarefa: TDataSource;
    qryContatoPara: TZQuery;
    dtsContatoPara: TDataSource;
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ConectaBD(PsNomeArquivo: string = '');
  end;

var
  dmTaskBan: TdmTaskBan;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

// --- M�TODOS DE USU�RIO ----------

{**
 * Verifica se o arquivo do banco de dados existe.
 * Se n�o existe, cria.
 *}
procedure TdmTaskBan.ConectaBD(PsNomeArquivo: string = '');
var
  sArqDB: string;
begin
  conTaskBan.Disconnect();
  conTaskBan.LibraryLocation := ExtractFilePath(ParamStr(0)) + C_DBLIB_PATH;
  if PsNomeArquivo <> '' then
    sArqDB:= ExtractFilePath(ParamStr(0)) + PsNomeArquivo
  else
    sArqDB:= ExtractFilePath(ParamStr(0)) + C_ARQ_DB;
  ForceDirectories(ExtractFilePath(sArqDB));
  conTaskBan.Database := sArqDB;
  // Se o DB n�o existe, executa o script de cria��o
  if not FileExists( sArqDB ) then
  begin
    conTaskBan.Connect; // Cria o arquivo zerado
    sqlProcCreateDB.Execute; // Cria as tabelas e registros b�sicos
  end
  else
    conTaskBan.Connect; // S� conecta
end;

// --- FIM DOS M�TODOS DE USU�RIO ----------

procedure TdmTaskBan.DataModuleDestroy(Sender: TObject);
begin
  if conTaskBan.Connected then
    conTaskBan.Disconnect;
end;

end.
