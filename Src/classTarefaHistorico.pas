unit classTarefaHistorico;


interface
uses
  Classes, SysUtils, DB, ZConnection, ZDataSet, Variants;

const
  C_TAREFA_HISTORICO_LIST_SQL = 'SELECT * FROM tarefahistorico WHERE idtarefa = :idtarefa ';

type
  TTarefaHistorico = class
  private
    FIDTarefa: integer;

    FTarefaHistoricoDS: TZQuery;

    function GetTarefaHistoricoDS: TZQuery;

  public
    property IDTarefa: integer read FIDTarefa write FIDTarefa;

    property TarefaHistoricoDS: TZQuery read GetTarefaHistoricoDS write FTarefaHistoricoDS;

    constructor Create();
    function LoadList(PiIDTarefa: integer): TDataSet;
    function Save: boolean;
    function Delete: boolean;

  end;


implementation

uses TaskBanDM;


{ TTarefa }


constructor TTarefaHistorico.Create;
begin
  inherited Create;
  FIDTarefa := 0;
end;

function TTarefaHistorico.GetTarefaHistoricoDS(): TZQuery;
begin
  if not Assigned(FTarefaHistoricoDS) then
  begin
    FTarefaHistoricoDS:= TZQuery.Create(dmTaskBan);
    FTarefaHistoricoDS.Connection:= dmTaskBan.conTaskBan;
    FTarefaHistoricoDS.SQL.Text := C_TAREFA_HISTORICO_LIST_SQL;
  end;
  Result := FTarefaHistoricoDS;
end;

function TTarefaHistorico.Save(): boolean;
begin
  Result:= false;

  // ToDo: Valida��es

  FTarefaHistoricoDS.Post;

  Result:= true; // Se chegou at� aqui, deu certo
end;

function TTarefaHistorico.Delete(): boolean;
begin
  Result:= false;
  FTarefaHistoricoDS.Delete();
  Result:= true;
end;


end.

