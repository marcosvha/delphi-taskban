{-----------------------------------------------------------------------------
 Unit Name: classTarefa
 Author:    Marcos VHA
 Date:      2017
 Purpose:   Classe que implementa uma Tarefa.
-----------------------------------------------------------------------------}
unit classTarefa;


interface
uses
  Classes, SysUtils, DB, ZConnection, ZDataSet, Variants;

const
  C_STATUS_ARRAY: array[1..3] of string = ('P', 'A', 'C');
  C_STATUSNOME_ARRAY: array[1..3] of string = ('PENDENTE', 'ANDAMENTO', 'CONCLU�DA');

  C_TAREFA_SQL = 'SELECT * FROM tarefa WHERE idtarefa = :idtarefa ';
  C_TAREFA_LIST_SQL = 'SELECT * FROM tarefa ';
  C_TAREFA_HISTORICO_LIST_SQL = 'SELECT * FROM tarefahistorico WHERE idtarefa = :idtarefa ';

type
  TTarefa = class
  private
    FID: integer;
    Fstatus: string; // C_STATUS_ARRAY: P=PENDENTE; A=ANDAMENTO; C=CONCLU�DA;
    Fstatusnome: string;
    Ftitulo: string;
    Fdesc: WideString;
    Fdatacriacao: TDateTime;
    Fdatatermino: TDateTime;
    Fdatavencimento: TDateTime;
    Ftempoestimado: real;
    Ftempodedicado: real;
    Ftemporestante: real;
    Fidcategoriatarefa: integer;
    Fcategoriatarefanome: string;
    Fidcontato_responsavel: integer;
    Fcontatonome_responsavel: string;
    Fidcontato_para: integer;
    Fcontatonome_para: string;

    FTarefaDS: TZQuery;
    FTarefaHistoricoDS: TZQuery;
    function GetTarefaDS: TZQuery;
    function GetTarefaHistoricoDS: TZQuery;

    procedure SetStatus(const Value: String);

  public
    class function GetStatusColor(PsStatus: string): string;

    property ID: integer read FID write FID;
    property status: string read Fstatus write SetStatus; // C�digo do status: P, A, C
    property statusnome: string read Fstatusnome; // Descri��o do status: Pendente, Andamento, Concluido
    property titulo: string read Ftitulo write Ftitulo;
    property desc: WideString read Fdesc write Fdesc;
    property datacriacao: TDateTime read Fdatacriacao write Fdatacriacao;
    property datatermino: TDateTime read Fdatatermino write Fdatatermino;
    property datavencimento: TDateTime read Fdatavencimento write Fdatavencimento;
    property tempoestimado: real read Ftempoestimado write Ftempoestimado;
    property tempodedicado: real read Ftempodedicado write Ftempodedicado;
    property temporestante: real read Ftemporestante write Ftemporestante;
    property idcategoriatarefa: integer read Fidcategoriatarefa write Fidcategoriatarefa;
    property categoriatarefanome: string read Fcategoriatarefanome;
    property idcontato_responsavel: integer read Fidcontato_responsavel write Fidcontato_responsavel;
    property contatonome_responsavel: string read Fcontatonome_responsavel;
    property idcontato_para: integer read Fidcontato_para write Fidcontato_para;
    property contatonome_para: string read Fcontatonome_para;

    property TarefaDS: TZQuery read GetTarefaDS write FTarefaDS;
    property TarefaHistoricoDS: TZQuery read GetTarefaHistoricoDS write FTarefaHistoricoDS;

    constructor Create();
    function Load(PiIDTarefa: integer): TDataSet;
    function Save: boolean;
    function Delete: boolean;

    function AddHistorico(PsTipo: string; PiIDContato: Integer; PsCampoAlterado: string;
      PsValorAntigo, PsValorNovo: WideString): boolean;
    function AddComentario(PiIdContato: integer; PsComentario: WideString): boolean;
    function LoadHistorico(PcTipo: String = ''; PiIDContato: integer = 0): TDataSet;
  end;


implementation

uses TaskBanDM;


{ TTarefa }


constructor TTarefa.Create;
begin
  inherited Create;

  FID := 0;
  Fstatus := 'P'; // C�digo do status: P, A, C
  Ftitulo := '';
  Fdesc := '';
  Fdatacriacao := now();
  Fdatatermino := 0;
  Fdatavencimento := 0;
  Ftempoestimado := 0;
  Ftempodedicado := 0;
  Ftemporestante := 0;
  Fidcategoriatarefa := 0;
  Fidcontato_responsavel := 0;
  Fidcontato_para := 0;

end;

{-----------------------------------------------------------------------------
  Procedure: GetTarefaDS
  Author:    Marcos VHA
  Date:      2017
  Purpose:   Retorna um dataset (fechado) com SQL para sele��o de uma tarefa.
  Result:    TDataSet
-----------------------------------------------------------------------------}
function TTarefa.GetTarefaDS(): TZQuery;
begin
  if not Assigned(FTarefaDS) then
  begin
    FTarefaDS:= TZQuery.Create(dmTaskBan);
    FTarefaDS.Connection:= dmTaskBan.conTaskBan;
    FTarefaDS.SQL.Text := C_TAREFA_SQL;
  end;
  Result := FTarefaDS;
end;

{-----------------------------------------------------------------------------
  Procedure: GetTarefaHistoricoDS
  Author:    Marcos VHA
  Date:      2017
  Purpose:   Retorna um dataset (fechado) com SQL para sele��o da lista de
              hist�rico/coment�rios de uma tarefa.
  Result:    TDataSet
-----------------------------------------------------------------------------}
function TTarefa.GetTarefaHistoricoDS: TZQuery;
begin
  if not Assigned(FTarefaHistoricoDS) then
  begin
    FTarefaHistoricoDS:= TZQuery.Create(dmTaskBan);
    FTarefaHistoricoDS.Connection:= dmTaskBan.conTaskBan;
    FTarefaHistoricoDS.SQL.Text := C_TAREFA_HISTORICO_LIST_SQL;
  end;
  Result := FTarefaHistoricoDS;
end;


{-----------------------------------------------------------------------------
  Procedure: Load
  Author:    Marcos VHA
  Date:      2017
  Purpose:   Carrega um registro correspondente ao par�metro PiIDTarefa.
  Arguments: PiIDTarefa: Chave prim�ria.
  Result:    boolean
-----------------------------------------------------------------------------}
function TTarefa.Load(PiIDTarefa: integer): TDataSet;
begin
  Result:= nil;
  FID:= PiIDTarefa;
  GetTarefaDS(); // Para inicializar FTarefaDS
  if FTarefaDS.Active then
    FTarefaDS.Close;
  FTarefaDS.ParamByName('idtarefa').AsInteger := FID;
  FTarefaDS.Open;

  if not (FTarefaDS.BOF and FTarefaDS.EOF) then
  begin
    Fstatus := FTarefaDS.FieldByName('tarefastatus').AsString;
    Ftitulo := FTarefaDS.FieldByName('tarefanome').AsString;
    Fdesc := FTarefaDS.FieldByName('tarefadesc').AsWideString;
    Fdatacriacao := FTarefaDS.FieldByName('tarefadatacriacao').AsDateTime;
    Fdatatermino := FTarefaDS.FieldByName('tarefadatatermino').AsDateTime;
    Fdatavencimento := FTarefaDS.FieldByName('tarefadatavencimento').AsDateTime;
    Ftempoestimado := FTarefaDS.FieldByName('tarefatempoestimado').AsFloat;
    Ftempodedicado := FTarefaDS.FieldByName('tarefatempodedicado').AsFloat;
    Ftemporestante := FTarefaDS.FieldByName('tarefatemporestante').AsFloat;
    Fidcategoriatarefa := FTarefaDS.FieldByName('idcategoriatarefa').AsInteger;
    Fidcontato_responsavel := FTarefaDS.FieldByName('idcontato_responsavel').AsInteger;
    Fidcontato_para := FTarefaDS.FieldByName('idcontato_pessoa').AsInteger;
    // Todo: trazer os campos nome, join
  end;

  Result:= FTarefaDS;
end;

{-----------------------------------------------------------------------------
  Procedure: Save
  Author:    Marcos VHA
  Date:      2017
  Purpose:   Persiste os atributos da classe na tabela tarefa.
             Obs.: FTarefaDS precisa estar em estado dsInsert ou dsEdit.
             Em caso de edi��o, FTarefaDS carregado (Load) com o registro a ser alterado.
  Result:    boolean
-----------------------------------------------------------------------------}
function TTarefa.Save(): boolean;
begin
  Result:= false;
  // ToDo: Valida��es
  if not Assigned(FTarefaDS) then
    GetTarefaDS(); // Para inicializar FTarefaDS
  if not FTarefaDS.Active then
    FTarefaDS.Open();
  if not (FTarefaDS.State in [dsInsert, dsEdit]) then
    if (FID > 0) then
      FTarefaDS.Edit
    else
      FTarefaDS.Insert;
  if (FTarefaDS.FieldByName('tarefadatacriacao').IsNull) then
    FTarefaDS.FieldByName('tarefadatacriacao').AsDateTime := Fdatacriacao;
  FTarefaDS.FieldByName('tarefastatus').AsString := Fstatus;
  FTarefaDS.FieldByName('tarefanome').AsString := Ftitulo;
  FTarefaDS.FieldByName('tarefadesc').AsWideString := Fdesc;
  if (Fdatatermino > 0) then
    FTarefaDS.FieldByName('tarefadatatermino').AsDateTime := Fdatatermino
  else
    FTarefaDS.FieldByName('tarefadatatermino').Clear;
  if (Fdatavencimento > 0) then
    FTarefaDS.FieldByName('tarefadatavencimento').AsDateTime := Fdatavencimento
  else
    FTarefaDS.FieldByName('tarefadatavencimento').Clear;
  FTarefaDS.FieldByName('tarefatempoestimado').AsFloat := Ftempoestimado;
  FTarefaDS.FieldByName('tarefatempodedicado').AsFloat := Ftempodedicado;
  FTarefaDS.FieldByName('tarefatemporestante').AsFloat := Ftemporestante;
  FTarefaDS.FieldByName('idcategoriatarefa').AsInteger := Fidcategoriatarefa;
  FTarefaDS.FieldByName('idcontato_responsavel').AsInteger := Fidcontato_responsavel;
  FTarefaDS.FieldByName('idcontato_pessoa').AsInteger := Fidcontato_para;
  FTarefaDS.Post;

  // Pega o ID da tarefa criada/alterada
  if FID <= 0 then
    FID := FTarefaDS.FieldByName('idtarefa').AsInteger;

  Result:= true; // Se chegou at� aqui, deu certo
end;

{-----------------------------------------------------------------------------
  Procedure: Delete
  Author:    Marcos VHA
  Date:      2017
  Purpose:   Exclui um registro da tabela tarefa.
             Obs.: FTarefaDS precisa estar em carregado (Load) com o registro a
             ser exclu�do.
  Result:    boolean
-----------------------------------------------------------------------------}
function TTarefa.Delete(): boolean;
begin
  Result:= false;
  FTarefaDS.Delete();
  Result:= true;
end;

function TTarefa.AddHistorico(PsTipo: string; PiIDContato: Integer; PsCampoAlterado: string;
  PsValorAntigo, PsValorNovo: WideString): boolean;
begin
  Result:= false;
  if not (TarefaHistoricoDS.Active) then
    TarefaHistoricoDS.Open;
  TarefaHistoricoDS.Insert;
  TarefaHistoricoDS.FieldByName('idtarefa').AsInteger := FID;
  TarefaHistoricoDS.FieldByName('idcontato').AsInteger := PiIdContato;
  TarefaHistoricoDS.FieldByName('tarefahistoricodataalteracao').AsDateTime := now();
  TarefaHistoricoDS.FieldByName('tarefahistoricocampoalterado').AsString := PsCampoAlterado;
  TarefaHistoricoDS.FieldByName('tarefahistoricovalorantigo').AsString := PsValorAntigo;
  TarefaHistoricoDS.FieldByName('tarefahistoricovalornovo').AsString := PsValorNovo;
  TarefaHistoricoDS.FieldByName('tarefahistoricotipo').AsString := PsTipo; // H=Hist�rico de altera��o; C=Comentario
  TarefaHistoricoDS.Post;
  Result:= true;
end;

function TTarefa.AddComentario(PiIdContato: integer; PsComentario: WideString): boolean;
begin
  Result:= false;
  Result:= Self.AddHistorico('C', PiIdContato, 'comentario', '', PsComentario);
end;


function TTarefa.LoadHistorico(PcTipo: String = ''; PiIDContato: integer = 0): TDataSet;
begin
  Result:= nil;
  FTarefaHistoricoDS := nil;
  TarefaHistoricoDS.ParamByName('idtarefa').AsInteger := FID;
  if (PiIDContato > 0) then
  begin
    TarefaHistoricoDS.SQL.Text := TarefaHistoricoDS.SQL.Text + ' AND idcontato = :idcontato';
    TarefaHistoricoDS.ParamByName('idcontato').AsInteger := PiIDContato;
  end;
  if (PcTipo <> '') then
  begin
    TarefaHistoricoDS.SQL.Text := TarefaHistoricoDS.SQL.Text + ' AND tarefahistoricotipo = :tarefahistoricotipo';
    TarefaHistoricoDS.ParamByName('tarefahistoricotipo').AsString := PcTipo;
  end;

  TarefaHistoricoDS.SQL.Text := TarefaHistoricoDS.SQL.Text + ' ORDER BY tarefahistoricodataalteracao DESC';

  TarefaHistoricoDS.Open;
  Result:= TarefaHistoricoDS;
end;

{-----------------------------------------------------------------------------
  Procedure: GetStatusColor
  Author:    Marcos VHA
  Date:      Summer2015
  Purpose:   Retornar o c�digo Hexa da cor que representa a situa��o da nota ou item.
  Arguments: PsStatus: string = 'A'
  Result:    string
-----------------------------------------------------------------------------}
class function TTarefa.GetStatusColor(PsStatus: string): string;
begin

  // ToDo: ajustar para ficar conforme a data de validade da tarefa

  // Esse trecho � s� um snippet
    case (Ord(PsStatus[1])) of
      Ord('A'): Result:= 'b92c28'; // Aberto = Vermelho escuro
      Ord('P'): Result:= 'e38d13'; // em Produ��o = Amarelo
      Ord('L'): Result:= '3e8f3e'; // Liberado = Verde
      Ord('E'): Result:= 'd9edf7'; // Entregue = Azul claro
      Ord('F'): Result:= 'd9edf7'; // Fechado = Azul claro
      Ord('C'): Result:= 'c0c0c0'; // Cancelado = Cinza
      Ord('R'): Result:= 'f2dede'; // Rejeitado = Vermelho claro
      Ord('X'): Result:= 'f2dede'; // Comanda Perdida = claro
    end;
end;

procedure TTarefa.SetStatus(const Value: String);
var
  sOldStatus: string;
  dtOldDataTermino: TDateTime;
begin
  sOldStatus := Fstatus;
  Fstatus := Value;

  // ToDo: Pegar o indice e setar a descri��o
  {
  C_STATUS_ARRAY      SetStatus
  case Fstatus of
    'P':
  end;
  }
  if (sOldStatus <> Fstatus) then
    Self.AddHistorico('H', Fidcontato_responsavel, 'tarefastatus', sOldStatus, Fstatus);
  if (Value = 'C') and
     (Fdatatermino = 0) then
  begin
    // Se Conclu�da, atribui data de t�rmino = agora
    dtOldDataTermino := Fdatatermino;
    Fdatatermino := Now();
    Self.AddHistorico('H', Fidcontato_responsavel, 'tarefadatatermino', DateTimeToStr(dtOldDataTermino), DateTimeToStr(Fdatatermino));
  end
  else
    if (Value <> 'C') and
       (Fdatatermino > 0) then
    begin
      // Se n�o Conclu�da, limpa data de t�rmino
      dtOldDataTermino := Fdatatermino;
      Fdatatermino := 0;
      Self.AddHistorico('H', Fidcontato_responsavel, 'tarefadatatermino', DateTimeToStr(dtOldDataTermino), DateTimeToStr(Fdatatermino));
    end;
end;




end.

