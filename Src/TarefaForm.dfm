﻿object frmTarefa: TfrmTarefa
  Left = 0
  Top = 0
  Caption = 'Tarefa'
  ClientHeight = 559
  ClientWidth = 623
  Color = clBtnFace
  Constraints.MinHeight = 550
  Constraints.MinWidth = 625
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnDestroy = FormDestroy
  OnShow = FormShow
  DesignSize = (
    623
    559)
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 607
    Height = 312
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Detalhes da Tarefa'
    TabOrder = 0
    ExplicitWidth = 608
    object Descrição: TLabel
      Left = 11
      Top = 139
      Width = 46
      Height = 13
      Caption = 'Descri'#231#227'o'
    end
    object Label1: TLabel
      Left = 415
      Top = 58
      Width = 60
      Height = 13
      Caption = 'Data cria'#231#227'o'
    end
    object Label2: TLabel
      Left = 415
      Top = 99
      Width = 81
      Height = 13
      Caption = 'Data vencimento'
    end
    object Label3: TLabel
      Left = 415
      Top = 257
      Width = 62
      Height = 13
      Caption = 'Data t'#233'rmino'
    end
    object Label4: TLabel
      Left = 10
      Top = 58
      Width = 47
      Height = 13
      Caption = 'Categoria'
    end
    object Label5: TLabel
      Left = 161
      Top = 58
      Width = 61
      Height = 13
      Caption = 'Respons'#225'vel'
    end
    object Label6: TLabel
      Left = 10
      Top = 100
      Width = 22
      Height = 13
      Caption = 'Para'
    end
    object edtTitulo: TLabeledEdit
      Left = 9
      Top = 34
      Width = 400
      Height = 21
      EditLabel.Width = 74
      EditLabel.Height = 13
      EditLabel.Caption = 'T'#237'tulo da tarefa'
      TabOrder = 0
    end
    object memDesc: TMemo
      Left = 9
      Top = 156
      Width = 400
      Height = 98
      ScrollBars = ssVertical
      TabOrder = 4
    end
    object edtTempoEstimado: TLabeledEdit
      Left = 415
      Top = 156
      Width = 104
      Height = 21
      EditLabel.Width = 101
      EditLabel.Height = 13
      EditLabel.Caption = 'Tempo estimado (Hs)'
      NumbersOnly = True
      TabOrder = 11
      Text = '0'
    end
    object edtTempoDedicado: TLabeledEdit
      Left = 415
      Top = 194
      Width = 104
      Height = 21
      EditLabel.Width = 101
      EditLabel.Height = 13
      EditLabel.Caption = 'Tempo dedicado (Hs)'
      NumbersOnly = True
      TabOrder = 12
      Text = '0'
    end
    object edtTempoRestante: TLabeledEdit
      Left = 415
      Top = 230
      Width = 104
      Height = 21
      EditLabel.Width = 99
      EditLabel.Height = 13
      EditLabel.Caption = 'Tempo restante (Hs)'
      NumbersOnly = True
      TabOrder = 13
      Text = '0'
    end
    object edtDataCriacao: TDateTimePicker
      Left = 415
      Top = 74
      Width = 98
      Height = 21
      Date = 0.931297939816431600
      Time = 0.931297939816431600
      Enabled = False
      TabOrder = 7
      OnChange = edtDataCriacaoChange
    end
    object edtHoraCriacao: TDateTimePicker
      Left = 519
      Top = 74
      Width = 74
      Height = 21
      Date = 42833.000000000000000000
      Time = 42833.000000000000000000
      Enabled = False
      Kind = dtkTime
      TabOrder = 8
    end
    object edtDataVencimento: TDateTimePicker
      Left = 415
      Top = 113
      Width = 98
      Height = 21
      Date = 42833.931297939810000000
      Format = ' '
      Time = 42833.931297939810000000
      TabOrder = 9
      OnChange = edtDataCriacaoChange
      OnEnter = edtDataVencimentoEnter
    end
    object edtHoraVencimento: TDateTimePicker
      Left = 519
      Top = 113
      Width = 74
      Height = 21
      Date = 42833.999988425930000000
      Time = 42833.999988425930000000
      Kind = dtkTime
      TabOrder = 10
      Visible = False
    end
    object edtDataTermino: TDateTimePicker
      Left = 415
      Top = 272
      Width = 98
      Height = 21
      Date = 42833.931297939810000000
      Format = ' '
      Time = 42833.931297939810000000
      TabOrder = 14
      OnChange = edtDataCriacaoChange
      OnEnter = edtDataTerminoEnter
    end
    object edtHoraTermino: TDateTimePicker
      Left = 519
      Top = 272
      Width = 74
      Height = 21
      Date = 42833.000000000000000000
      Time = 42833.000000000000000000
      Kind = dtkTime
      TabOrder = 15
      Visible = False
    end
    object rdgSituacao: TRadioGroup
      Left = 11
      Top = 260
      Width = 398
      Height = 42
      Caption = 'Situa'#231#227'o'
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        'PENDENTE'
        'ANDAMENTO'
        'CONCLU'#205'DA')
      TabOrder = 5
    end
    object edtIDTarefa: TLabeledEdit
      Left = 415
      Top = 33
      Width = 104
      Height = 21
      EditLabel.Width = 43
      EditLabel.Height = 13
      EditLabel.Caption = 'Tarefa #'
      NumbersOnly = True
      ReadOnly = True
      TabOrder = 6
      Text = '0'
    end
    object cbxCategoria: TDBLookupComboBox
      Left = 10
      Top = 73
      Width = 145
      Height = 21
      KeyField = 'idcategoriatarefa'
      ListField = 'categoriatarefanome'
      ListSource = dmTaskBan.dtsCategoriaTarefa
      TabOrder = 1
    end
    object cbxResponsavel: TDBLookupComboBox
      Left = 161
      Top = 73
      Width = 248
      Height = 21
      KeyField = 'idcontato'
      ListField = 'contatonome'
      ListSource = dmTaskBan.dtsContato
      TabOrder = 2
    end
    object cbxPara: TDBLookupComboBox
      Left = 10
      Top = 113
      Width = 399
      Height = 21
      KeyField = 'idcontato'
      ListField = 'contatonome'
      ListSource = dmTaskBan.dtsContatoPara
      TabOrder = 3
    end
  end
  object GroupBox3: TGroupBox
    Left = 8
    Top = 407
    Width = 607
    Height = 103
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = 'Hist'#243'rico'
    TabOrder = 1
    ExplicitWidth = 608
    DesignSize = (
      607
      103)
    object memHistorico: TMemo
      Left = 7
      Top = 20
      Width = 586
      Height = 80
      Anchors = [akLeft, akTop, akRight, akBottom]
      ReadOnly = True
      ScrollBars = ssVertical
      TabOrder = 0
    end
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 324
    Width = 607
    Height = 74
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Adicionar coment'#225'rio'
    TabOrder = 2
    ExplicitWidth = 608
    DesignSize = (
      607
      74)
    object memAddComentario: TMemo
      Left = 7
      Top = 16
      Width = 506
      Height = 50
      Anchors = [akLeft, akTop, akRight, akBottom]
      ScrollBars = ssVertical
      TabOrder = 0
    end
    object btnAddComentario: TButton
      Left = 519
      Top = 41
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Comentar'
      TabOrder = 1
      OnClick = btnAddComentarioClick
    end
  end
  object btnOK: TButton
    Left = 459
    Top = 526
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    TabOrder = 3
    OnClick = btnOKClick
    ExplicitTop = 479
  end
  object btnCancelar: TButton
    Left = 540
    Top = 526
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Cancelar'
    ModalResult = 2
    TabOrder = 4
    ExplicitTop = 479
  end
end
