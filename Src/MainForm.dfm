object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = 'TaskBan'
  ClientHeight = 606
  ClientWidth = 1015
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  DesignSize = (
    1015
    606)
  PixelsPerInch = 96
  TextHeight = 13
  object Label11: TLabel
    Left = 24
    Top = 392
    Width = 37
    Height = 13
    Caption = 'Label11'
  end
  object pcTarefas: TPageControl
    Left = 8
    Top = 56
    Width = 999
    Height = 540
    ActivePage = tabGrid
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    object tabKanban: TTabSheet
      Caption = 'Quadro de Tarefas'
      ExplicitLeft = 0
      ExplicitWidth = 1001
      ExplicitHeight = 557
      object Label1: TLabel
        Left = 8
        Top = 10
        Width = 87
        Height = 26
        Caption = 'Pendente:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TLabel
        Left = 337
        Top = 10
        Width = 106
        Height = 26
        Caption = 'Andamento:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
      end
      object Label3: TLabel
        Left = 669
        Top = 10
        Width = 91
        Height = 26
        Caption = 'Conclu'#237'do:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
      end
      object lblToDoCount: TLabel
        Left = 256
        Top = 19
        Width = 44
        Height = 13
        Alignment = taRightJustify
        Caption = '0 tarefas'
      end
      object lblDoingCount: TLabel
        Left = 593
        Top = 19
        Width = 44
        Height = 13
        Alignment = taRightJustify
        Caption = '0 tarefas'
      end
      object lblDoneCount: TLabel
        Left = 925
        Top = 19
        Width = 44
        Height = 13
        Alignment = taRightJustify
        Caption = '0 tarefas'
      end
      object ctgToDo: TDBCtrlGrid
        Left = 0
        Top = 42
        Width = 300
        Height = 72
        AllowDelete = False
        AllowInsert = False
        Color = clInfoBk
        DataSource = dtsToDo
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        PanelWidth = 283
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        RowCount = 1
        OnDblClick = ctgToDoDblClick
        OnDragDrop = ctgToDoDragDrop
        OnDragOver = ctgDoingDragOver
        OnMouseDown = ctgToDoMouseDown
        object DBText1: TDBText
          Left = 3
          Top = 23
          Width = 252
          Height = 18
          Anchors = [akLeft, akTop, akRight]
          DataField = 'tarefanome'
          DataSource = dtsToDo
          ExplicitWidth = 337
        end
        object DBText2: TDBText
          Left = 4
          Top = 53
          Width = 97
          Height = 11
          DataField = 'tarefadatavencimento'
          DataSource = dtsToDo
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label4: TLabel
          Left = 3
          Top = 40
          Width = 52
          Height = 11
          Caption = 'Vencimento:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object DBText3: TDBText
          Left = 14
          Top = 4
          Width = 43
          Height = 18
          DataField = 'idtarefa'
          DataSource = dtsToDo
        end
        object Label5: TLabel
          Left = 3
          Top = 4
          Width = 8
          Height = 13
          Caption = '#'
        end
        object DBText4: TDBText
          Left = 63
          Top = 6
          Width = 89
          Height = 11
          DataField = 'tarefadatacriacao'
          DataSource = dtsToDo
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label13: TLabel
          Left = 256
          Top = -72
          Width = 37
          Height = 13
          Caption = 'Label13'
        end
        object Label15: TLabel
          Left = 123
          Top = 40
          Width = 22
          Height = 11
          Caption = 'Para:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object DBText14: TDBText
          Left = 123
          Top = 53
          Width = 89
          Height = 11
          DataField = 'contatonome_para'
          DataSource = dtsToDo
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
      end
      object ctgDoing: TDBCtrlGrid
        Left = 337
        Top = 42
        Width = 300
        Height = 72
        AllowDelete = False
        AllowInsert = False
        Color = clInfoBk
        DataSource = dtsDoing
        PanelWidth = 283
        ParentColor = False
        TabOrder = 1
        RowCount = 1
        OnDblClick = ctgToDoDblClick
        OnDragDrop = ctgDoingDragDrop
        OnDragOver = ctgDoingDragOver
        OnMouseDown = ctgToDoMouseDown
        object DBText5: TDBText
          Left = 4
          Top = 23
          Width = 284
          Height = 18
          Anchors = [akLeft, akTop, akRight]
          DataField = 'tarefanome'
          DataSource = dtsDoing
          ExplicitWidth = 337
        end
        object DBText7: TDBText
          Left = 14
          Top = 4
          Width = 43
          Height = 18
          DataField = 'idtarefa'
          DataSource = dtsDoing
        end
        object Label7: TLabel
          Left = 3
          Top = 4
          Width = 8
          Height = 13
          Caption = '#'
        end
        object DBText8: TDBText
          Left = 63
          Top = 6
          Width = 89
          Height = 11
          DataField = 'tarefadatacriacao'
          DataSource = dtsDoing
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label6: TLabel
          Left = 4
          Top = 40
          Width = 52
          Height = 11
          Caption = 'Vencimento:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object DBText6: TDBText
          Left = 4
          Top = 51
          Width = 101
          Height = 11
          DataField = 'tarefadatavencimento'
          DataSource = dtsDoing
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label16: TLabel
          Left = 132
          Top = 40
          Width = 22
          Height = 11
          Caption = 'Para:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object DBText15: TDBText
          Left = 133
          Top = 51
          Width = 124
          Height = 11
          DataField = 'contatonome_para'
          DataSource = dtsDoing
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
      end
      object ctgDone: TDBCtrlGrid
        Left = 669
        Top = 42
        Width = 300
        Height = 72
        AllowDelete = False
        AllowInsert = False
        Color = clInfoBk
        DataSource = dtsDone
        PanelWidth = 283
        ParentColor = False
        TabOrder = 2
        RowCount = 1
        OnDblClick = ctgToDoDblClick
        OnDragDrop = ctgDoneDragDrop
        OnDragOver = ctgDoingDragOver
        OnMouseDown = ctgToDoMouseDown
        object DBText9: TDBText
          Left = 3
          Top = 23
          Width = 347
          Height = 18
          Anchors = [akLeft, akTop, akRight]
          DataField = 'tarefanome'
          DataSource = dtsDone
          ExplicitWidth = 337
        end
        object DBText11: TDBText
          Left = 14
          Top = 4
          Width = 43
          Height = 18
          DataField = 'idtarefa'
          DataSource = dtsDone
        end
        object Label10: TLabel
          Left = 3
          Top = 4
          Width = 8
          Height = 13
          Caption = '#'
        end
        object DBText12: TDBText
          Left = 63
          Top = 6
          Width = 89
          Height = 11
          DataField = 'tarefadatacriacao'
          DataSource = dtsDone
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label8: TLabel
          Left = 179
          Top = 37
          Width = 45
          Height = 11
          Caption = 'Conclus'#227'o:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object DBText13: TDBText
          Left = 179
          Top = 51
          Width = 89
          Height = 11
          DataField = 'tarefadatatermino'
          DataSource = dtsDone
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label17: TLabel
          Left = 3
          Top = 38
          Width = 22
          Height = 11
          Caption = 'Para:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object DBText16: TDBText
          Left = 4
          Top = 51
          Width = 89
          Height = 11
          DataField = 'contatonome_pessoa'
          DataSource = dtsDone
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
      end
    end
    object tabGrid: TTabSheet
      Caption = 'Lista de Tarefas'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitWidth = 1016
      ExplicitHeight = 562
      DesignSize = (
        991
        512)
      object Label12: TLabel
        Left = 3
        Top = 362
        Width = 110
        Height = 13
        Caption = 'Hist'#243'rico de Altera'#231#245'es'
      end
      object Label14: TLabel
        Left = 503
        Top = 362
        Width = 60
        Height = 13
        Caption = 'Coment'#225'rios'
      end
      object grdTarefas: TDBGrid
        Left = 3
        Top = 0
        Width = 985
        Height = 292
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataSource = dtsTarefas
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnDblClick = grdTarefasDblClick
        Columns = <
          item
            Expanded = False
            FieldName = 'idtarefa'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'tarefadatacriacao'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'tarefastatus'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'tarefanome'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'idcategoriatarefa'
            Width = -1
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'categoriatarefanome'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'idcontato_pessoa'
            Width = -1
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'idcontato_responsavel'
            Width = -1
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'contatonome_responsavel'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'contatonome_para'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'tarefadesc'
            Width = -1
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'tarefadatavencimento'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'tarefatempoestimado'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'tarefatempodedicado'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'tarefatemporestante'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'tarefadatatermino'
            Width = 100
            Visible = True
          end>
      end
      object DBGrid2: TDBGrid
        Left = 3
        Top = 331
        Width = 494
        Height = 167
        Anchors = [akLeft, akBottom]
        DataSource = dtsTarefaHistorico
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'idtarefa'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'idcontato'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'tarefahistoricodataalteracao'
            Width = 75
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'tarefahistoricocampoalterado'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'tarefahistoricovalorantigo'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'tarefahistoricovalornovo'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'tarefahistoricotipo'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'contatonome'
            Title.Caption = 'Usu'#225'rio'
            Width = 100
            Visible = True
          end>
      end
      object DBGrid3: TDBGrid
        Left = 503
        Top = 331
        Width = 485
        Height = 167
        Anchors = [akLeft, akRight, akBottom]
        DataSource = dtsTarefaComentarios
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'idtarefa'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'idcontato'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'tarefahistoricodataalteracao'
            Title.Caption = 'Data'
            Width = 85
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'contatonome'
            Title.Caption = 'Usu'#225'rio'
            Width = 114
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'tarefahistoricovalornovo'
            Title.Caption = 'Coment'#225'rio'
            Width = 254
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'tarefahistoricotipo'
            Visible = False
          end>
      end
    end
  end
  object btnAddTarefa: TButton
    Left = 8
    Top = 17
    Width = 105
    Height = 33
    Caption = 'Nova tarefa...'
    TabOrder = 1
    OnClick = btnAddTarefaClick
  end
  object qryToDo: TZQuery
    Connection = dmTaskBan.conTaskBan
    AfterOpen = qryToDoAfterOpen
    AfterRefresh = qryToDoAfterOpen
    SQL.Strings = (
      'select * from tarefa'
      'where tarefastatus = '#39'P'#39)
    Params = <>
    Left = 176
    Top = 176
    object qryToDoidtarefa: TLargeintField
      DisplayLabel = '#'
      FieldName = 'idtarefa'
    end
    object qryToDotarefastatus: TWideStringField
      DisplayLabel = 'Situa'#231#227'o'
      FieldName = 'tarefastatus'
      Size = 1
    end
    object qryToDotarefanome: TWideStringField
      DisplayLabel = 'Tarefa'
      FieldName = 'tarefanome'
      Required = True
      Size = 255
    end
    object qryToDotarefadesc: TBlobField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'tarefadesc'
    end
    object qryToDotarefadatacriacao: TDateTimeField
      DisplayLabel = 'Data cria'#231#227'o'
      FieldName = 'tarefadatacriacao'
      Required = True
    end
    object qryToDotarefatempoestimado: TFloatField
      DisplayLabel = 'Tempo estimado'
      FieldName = 'tarefatempoestimado'
    end
    object qryToDotarefatempodedicado: TFloatField
      DisplayLabel = 'Tempo dedicado'
      FieldName = 'tarefatempodedicado'
    end
    object qryToDotarefatemporestante: TFloatField
      DisplayLabel = 'Tempo restante'
      FieldName = 'tarefatemporestante'
    end
    object qryToDotarefadatatermino: TDateTimeField
      DisplayLabel = 'Data t'#233'rmino'
      FieldName = 'tarefadatatermino'
    end
    object qryToDotarefadatavencimento: TDateTimeField
      DisplayLabel = 'Data vencimento'
      FieldName = 'tarefadatavencimento'
    end
    object qryToDoidcategoriatarefa: TLargeintField
      FieldName = 'idcategoriatarefa'
      Required = True
    end
    object qryToDocategoriatarefanome: TStringField
      DisplayLabel = 'Categoria'
      FieldKind = fkLookup
      FieldName = 'categoriatarefanome'
      LookupDataSet = dmTaskBan.qryCategoriaTarefa
      LookupKeyFields = 'idcategoriatarefa'
      LookupResultField = 'categoriatarefanome'
      KeyFields = 'idcategoriatarefa'
      Size = 255
      Lookup = True
    end
    object qryToDoidcontato_pessoa: TLargeintField
      FieldName = 'idcontato_pessoa'
      Required = True
    end
    object qryToDoidcontato_responsavel: TLargeintField
      FieldName = 'idcontato_responsavel'
      Required = True
    end
    object qryToDoresponsavelnome: TStringField
      DisplayLabel = 'De'
      FieldKind = fkLookup
      FieldName = 'contatonome_responsavel'
      LookupDataSet = dmTaskBan.qryContato
      LookupKeyFields = 'idcontato'
      LookupResultField = 'contatonome'
      KeyFields = 'idcontato_responsavel'
      Size = 255
      Lookup = True
    end
    object qryToDopessoanome: TStringField
      DisplayLabel = 'Para'
      FieldKind = fkLookup
      FieldName = 'contatonome_para'
      LookupDataSet = dmTaskBan.qryContatoPara
      LookupKeyFields = 'idcontato'
      LookupResultField = 'contatonome'
      KeyFields = 'idcontato_pessoa'
      Size = 255
      Lookup = True
    end
  end
  object dtsToDo: TDataSource
    DataSet = qryToDo
    Left = 176
    Top = 225
  end
  object qryDoing: TZQuery
    Connection = dmTaskBan.conTaskBan
    AfterOpen = qryDoingAfterOpen
    AfterRefresh = qryDoingAfterOpen
    SQL.Strings = (
      'select * from tarefa'
      'where tarefastatus = '#39'A'#39)
    Params = <>
    Left = 544
    Top = 184
    object LargeintField1: TLargeintField
      DisplayLabel = '#'
      FieldName = 'idtarefa'
    end
    object WideStringField1: TWideStringField
      DisplayLabel = 'Situa'#231#227'o'
      FieldName = 'tarefastatus'
      Size = 1
    end
    object WideStringField2: TWideStringField
      DisplayLabel = 'Tarefa'
      FieldName = 'tarefanome'
      Required = True
      Size = 255
    end
    object BlobField1: TBlobField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'tarefadesc'
    end
    object DateTimeField1: TDateTimeField
      DisplayLabel = 'Data cria'#231#227'o'
      FieldName = 'tarefadatacriacao'
      Required = True
    end
    object FloatField1: TFloatField
      DisplayLabel = 'Tempo estimado'
      FieldName = 'tarefatempoestimado'
    end
    object FloatField2: TFloatField
      DisplayLabel = 'Tempo dedicado'
      FieldName = 'tarefatempodedicado'
    end
    object FloatField3: TFloatField
      DisplayLabel = 'Tempo restante'
      FieldName = 'tarefatemporestante'
    end
    object DateTimeField2: TDateTimeField
      DisplayLabel = 'Data t'#233'rmino'
      FieldName = 'tarefadatatermino'
    end
    object DateTimeField3: TDateTimeField
      DisplayLabel = 'Data vencimento'
      FieldName = 'tarefadatavencimento'
    end
    object LargeintField2: TLargeintField
      FieldName = 'idcategoriatarefa'
      Required = True
    end
    object StringField1: TStringField
      DisplayLabel = 'Categoria'
      FieldKind = fkLookup
      FieldName = 'categoriatarefanome'
      LookupDataSet = dmTaskBan.qryCategoriaTarefa
      LookupKeyFields = 'idcategoriatarefa'
      LookupResultField = 'categoriatarefanome'
      KeyFields = 'idcategoriatarefa'
      Size = 255
      Lookup = True
    end
    object LargeintField3: TLargeintField
      FieldName = 'idcontato_responsavel'
      Required = True
    end
    object LargeintField4: TLargeintField
      FieldName = 'idcontato_pessoa'
      Required = True
    end
    object qryDoingresponsavelnome: TStringField
      DisplayLabel = 'De'
      FieldKind = fkLookup
      FieldName = 'contatonome_responsavel'
      LookupDataSet = dmTaskBan.qryContato
      LookupKeyFields = 'idcontato'
      LookupResultField = 'contatonome'
      KeyFields = 'idcontato_responsavel'
      Size = 255
      Lookup = True
    end
    object qryDoingpessoanome: TStringField
      DisplayLabel = 'Para'
      FieldKind = fkLookup
      FieldName = 'contatonome_para'
      LookupDataSet = dmTaskBan.qryContatoPara
      LookupKeyFields = 'idcontato'
      LookupResultField = 'contatonome'
      KeyFields = 'idcontato_pessoa'
      Size = 255
      Lookup = True
    end
  end
  object dtsDoing: TDataSource
    DataSet = qryDoing
    Left = 544
    Top = 233
  end
  object qryDone: TZQuery
    Connection = dmTaskBan.conTaskBan
    AfterOpen = qryDoneAfterOpen
    AfterRefresh = qryDoneAfterOpen
    SQL.Strings = (
      'select * from tarefa'
      'where tarefastatus = '#39'C'#39)
    Params = <>
    Left = 896
    Top = 176
    object LargeintField5: TLargeintField
      DisplayLabel = '#'
      FieldName = 'idtarefa'
    end
    object WideStringField3: TWideStringField
      DisplayLabel = 'Situa'#231#227'o'
      FieldName = 'tarefastatus'
      Size = 1
    end
    object WideStringField4: TWideStringField
      DisplayLabel = 'Tarefa'
      FieldName = 'tarefanome'
      Required = True
      Size = 255
    end
    object BlobField2: TBlobField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'tarefadesc'
    end
    object DateTimeField4: TDateTimeField
      DisplayLabel = 'Data cria'#231#227'o'
      FieldName = 'tarefadatacriacao'
      Required = True
    end
    object FloatField4: TFloatField
      DisplayLabel = 'Tempo estimado'
      FieldName = 'tarefatempoestimado'
    end
    object FloatField5: TFloatField
      DisplayLabel = 'Tempo dedicado'
      FieldName = 'tarefatempodedicado'
    end
    object FloatField6: TFloatField
      DisplayLabel = 'Tempo restante'
      FieldName = 'tarefatemporestante'
    end
    object DateTimeField5: TDateTimeField
      DisplayLabel = 'Data t'#233'rmino'
      FieldName = 'tarefadatatermino'
    end
    object DateTimeField6: TDateTimeField
      DisplayLabel = 'Data vencimento'
      FieldName = 'tarefadatavencimento'
    end
    object LargeintField6: TLargeintField
      FieldName = 'idcategoriatarefa'
      Required = True
    end
    object StringField3: TStringField
      DisplayLabel = 'Categoria'
      FieldKind = fkLookup
      FieldName = 'categoriatarefanome'
      LookupDataSet = dmTaskBan.qryCategoriaTarefa
      LookupKeyFields = 'idcategoriatarefa'
      LookupResultField = 'categoriatarefanome'
      KeyFields = 'idcategoriatarefa'
      Size = 255
      Lookup = True
    end
    object LargeintField7: TLargeintField
      FieldName = 'idcontato_responsavel'
      Required = True
    end
    object LargeintField8: TLargeintField
      FieldName = 'idcontato_pessoa'
      Required = True
    end
    object StringField2: TStringField
      DisplayLabel = 'De'
      FieldKind = fkLookup
      FieldName = 'contatonome_responsavel'
      LookupDataSet = dmTaskBan.qryContato
      LookupKeyFields = 'idcontato'
      LookupResultField = 'contatonome'
      KeyFields = 'idcontato_responsavel'
      Size = 255
      Lookup = True
    end
    object StringField4: TStringField
      DisplayLabel = 'Para'
      FieldKind = fkLookup
      FieldName = 'contatonome_pessoa'
      LookupDataSet = dmTaskBan.qryContatoPara
      LookupKeyFields = 'idcontato'
      LookupResultField = 'contatonome'
      KeyFields = 'idcontato_pessoa'
      Size = 255
      Lookup = True
    end
  end
  object dtsDone: TDataSource
    DataSet = qryDone
    Left = 896
    Top = 225
  end
  object qryTarefas: TZQuery
    Connection = dmTaskBan.conTaskBan
    AfterOpen = qryToDoAfterOpen
    AfterRefresh = qryToDoAfterOpen
    SQL.Strings = (
      'select * from tarefa')
    Params = <>
    Left = 176
    Top = 296
    object LargeintField9: TLargeintField
      DisplayLabel = '#'
      FieldName = 'idtarefa'
    end
    object WideStringField5: TWideStringField
      DisplayLabel = 'Situa'#231#227'o'
      FieldName = 'tarefastatus'
      Size = 1
    end
    object WideStringField6: TWideStringField
      DisplayLabel = 'Tarefa'
      DisplayWidth = 50
      FieldName = 'tarefanome'
      Required = True
      Size = 255
    end
    object BlobField3: TBlobField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'tarefadesc'
    end
    object DateTimeField7: TDateTimeField
      DisplayLabel = 'Data cria'#231#227'o'
      FieldName = 'tarefadatacriacao'
      Required = True
    end
    object FloatField7: TFloatField
      DisplayLabel = 'Tempo estimado'
      FieldName = 'tarefatempoestimado'
    end
    object FloatField8: TFloatField
      DisplayLabel = 'Tempo dedicado'
      FieldName = 'tarefatempodedicado'
    end
    object FloatField9: TFloatField
      DisplayLabel = 'Tempo restante'
      FieldName = 'tarefatemporestante'
    end
    object DateTimeField8: TDateTimeField
      DisplayLabel = 'Data t'#233'rmino'
      FieldName = 'tarefadatatermino'
    end
    object DateTimeField9: TDateTimeField
      DisplayLabel = 'Data vencimento'
      FieldName = 'tarefadatavencimento'
    end
    object LargeintField10: TLargeintField
      FieldName = 'idcategoriatarefa'
      Required = True
    end
    object StringField5: TStringField
      DisplayLabel = 'Categoria'
      FieldKind = fkLookup
      FieldName = 'categoriatarefanome'
      LookupDataSet = dmTaskBan.qryCategoriaTarefa
      LookupKeyFields = 'idcategoriatarefa'
      LookupResultField = 'categoriatarefanome'
      KeyFields = 'idcategoriatarefa'
      Size = 255
      Lookup = True
    end
    object LargeintField11: TLargeintField
      FieldName = 'idcontato_pessoa'
      Required = True
    end
    object LargeintField12: TLargeintField
      FieldName = 'idcontato_responsavel'
      Required = True
    end
    object StringField6: TStringField
      DisplayLabel = 'De'
      DisplayWidth = 50
      FieldKind = fkLookup
      FieldName = 'contatonome_responsavel'
      LookupDataSet = dmTaskBan.qryContato
      LookupKeyFields = 'idcontato'
      LookupResultField = 'contatonome'
      KeyFields = 'idcontato_responsavel'
      Size = 255
      Lookup = True
    end
    object StringField7: TStringField
      DisplayLabel = 'Para'
      DisplayWidth = 50
      FieldKind = fkLookup
      FieldName = 'contatonome_para'
      LookupDataSet = dmTaskBan.qryContatoPara
      LookupKeyFields = 'idcontato'
      LookupResultField = 'contatonome'
      KeyFields = 'idcontato_pessoa'
      Size = 255
      Lookup = True
    end
  end
  object dtsTarefas: TDataSource
    DataSet = qryTarefas
    Left = 176
    Top = 345
  end
  object qryTarefaHistorico: TZQuery
    Connection = dmTaskBan.conTaskBan
    AfterOpen = qryToDoAfterOpen
    AfterRefresh = qryToDoAfterOpen
    SQL.Strings = (
      'select * from tarefahistorico'
      'where idtarefa = :idtarefa'
      'and tarefahistoricotipo = '#39'H'#39
      'order by tarefahistoricodataalteracao desc')
    Params = <
      item
        DataType = ftUnknown
        Name = 'idtarefa'
        ParamType = ptUnknown
      end>
    DataSource = dtsTarefas
    Left = 264
    Top = 296
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'idtarefa'
        ParamType = ptUnknown
      end>
    object qryTarefaHistoricoidtarefa: TLargeintField
      DisplayLabel = 'Tarefa #'
      FieldName = 'idtarefa'
    end
    object qryTarefaHistoricoidcontato: TLargeintField
      DisplayLabel = 'Contato #'
      FieldName = 'idcontato'
    end
    object qryTarefaHistoricotarefahistoricodataalteracao: TDateTimeField
      DisplayLabel = 'Data altera'#231#227'o'
      FieldName = 'tarefahistoricodataalteracao'
      Required = True
    end
    object qryTarefaHistoricotarefahistoricocampoalterado: TWideStringField
      DisplayLabel = 'Campo'
      FieldName = 'tarefahistoricocampoalterado'
      Required = True
      Size = 100
    end
    object qryTarefaHistoricotarefahistoricovalorantigo: TWideStringField
      DisplayLabel = 'Valor antigo'
      FieldName = 'tarefahistoricovalorantigo'
      Required = True
      Size = 255
    end
    object qryTarefaHistoricotarefahistoricovalornovo: TWideStringField
      DisplayLabel = 'Valor novo'
      FieldName = 'tarefahistoricovalornovo'
      Required = True
      Size = 255
    end
    object qryTarefaHistoricotarefahistoricotipo: TWideStringField
      DisplayLabel = 'Tipo'
      FieldName = 'tarefahistoricotipo'
      Required = True
      Size = 1
    end
    object qryTarefaHistoricocontatonome: TStringField
      FieldKind = fkLookup
      FieldName = 'contatonome'
      LookupDataSet = dmTaskBan.qryContato
      LookupKeyFields = 'idcontato'
      LookupResultField = 'contatonome'
      KeyFields = 'idcontato'
      Size = 255
      Lookup = True
    end
  end
  object dtsTarefaHistorico: TDataSource
    DataSet = qryTarefaHistorico
    Left = 264
    Top = 345
  end
  object qryTarefaComentarios: TZQuery
    Connection = dmTaskBan.conTaskBan
    AfterOpen = qryToDoAfterOpen
    AfterRefresh = qryToDoAfterOpen
    SQL.Strings = (
      'select * from tarefahistorico'
      'where idtarefa = :idtarefa'
      'and tarefahistoricotipo = '#39'C'#39
      'order by tarefahistoricodataalteracao desc')
    Params = <
      item
        DataType = ftUnknown
        Name = 'idtarefa'
        ParamType = ptUnknown
      end>
    DataSource = dtsTarefas
    Left = 368
    Top = 296
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'idtarefa'
        ParamType = ptUnknown
      end>
    object LargeintField13: TLargeintField
      DisplayLabel = 'Tarefa #'
      FieldName = 'idtarefa'
    end
    object LargeintField14: TLargeintField
      DisplayLabel = 'Contato #'
      FieldName = 'idcontato'
    end
    object DateTimeField10: TDateTimeField
      DisplayLabel = 'Data altera'#231#227'o'
      FieldName = 'tarefahistoricodataalteracao'
      Required = True
    end
    object WideStringField7: TWideStringField
      DisplayLabel = 'Campo'
      FieldName = 'tarefahistoricocampoalterado'
      Required = True
      Size = 100
    end
    object WideStringField8: TWideStringField
      DisplayLabel = 'Valor antigo'
      FieldName = 'tarefahistoricovalorantigo'
      Required = True
      Size = 255
    end
    object WideStringField9: TWideStringField
      DisplayLabel = 'Valor novo'
      FieldName = 'tarefahistoricovalornovo'
      Required = True
      Size = 255
    end
    object WideStringField10: TWideStringField
      DisplayLabel = 'Tipo'
      FieldName = 'tarefahistoricotipo'
      Required = True
      Size = 1
    end
    object qryTarefaComentarioscontatonome: TStringField
      FieldKind = fkLookup
      FieldName = 'contatonome'
      LookupDataSet = dmTaskBan.qryContato
      LookupKeyFields = 'idcontato'
      LookupResultField = 'contatonome'
      KeyFields = 'idcontato'
      Size = 255
      Lookup = True
    end
  end
  object dtsTarefaComentarios: TDataSource
    DataSet = qryTarefaComentarios
    Left = 368
    Top = 345
  end
end
