unit suiteTarefa;

interface
uses
  SysUtils,
  DUnitX.TestFramework,
  classTarefa,
  TaskBanDM,
  Data.DB;

type

  [TestFixture]
  TSuiteTarefa = class(TObject)
  private
    oTarefa: TTarefa;
  public
    [SetupFixture]
    procedure SuiteSetup;
    [TeardownFixture]
    procedure SuiteTeardown;

    [Setup]
    procedure Setup;
    [TearDown]
    procedure TearDown;

    [Test]
    [TestCase('CRCreate1','1,1,1')]
    [TestCase('CRCreate2','2,2,1')]
    procedure CRTarefaCreate(const PiIDCategoria, PiIDResponsavel, PiIDPara: Integer);
    [Test]
    [TestCase('CRRead1','1')]
    [TestCase('CRRead2','2')]
    procedure CRTarefaRead(const PiIDTarefa: Integer);
    [Test]
    [TestCase('CRUpdate1','1')]
    [TestCase('CRUpdate2','2')]
    procedure CRTarefaUpdate(const PiIDTarefa: Integer);
    [Test]
    [TestCase('CRDelete1','1')]
    [TestCase('CRDelete2','2')]
    procedure CRTarefaDelete(const PiIDTarefa: Integer);

    [Test]
    [TestCase('TestAddComentario1','1,Testando comentário em tarefa')]
    procedure TestAddComentario(const PiIDContato: Integer; PsComentario: WideString);
    [Test]
    [TestCase('TestAddHistoricoAlteracao1','1,tarefastatus,P,A')]
    procedure TestAddHistoricoAlteracao(const PiIDContato: Integer; PsCampoAlterado: string;
      PsValorAntigo, PsValorNovo: WideString);
    [Test]
    [TestCase('TestCarregaHistoricoCompleto','')]
    [TestCase('TestCarregaHistoricoAlteracoes','H,0')]
    [TestCase('TestCarregaComentarios','C,0')]
    [TestCase('TestCarregaHistoricoAlteracoesContato1','H,1')]
    [TestCase('TestCarregaComentariosContato1','C,1')]
    procedure TestCarregaHistorico(PcTipo: String = ''; PiIDContato: integer = 0);

    [Test]
    [TestCase('TestAlteraSituacaoA','A')] // Andamento
    [TestCase('TestAlteraSituacaoD','C')] // Concluída
    procedure TestAlteraSituacao(PsNovaSituacao: string);

  end;

implementation

{Inicialização da suite de testes}
procedure TSuiteTarefa.SuiteSetup;
begin
  dmTaskBan := TdmTaskBan.Create(nil);
  dmTaskBan.ConectaBD('Database\TaskBan_Testes.sqlite');
end;

{Finalização da suite de testes}
procedure TSuiteTarefa.SuiteTearDown;
begin
  dmTaskBan.Free;
  DeleteFile('Database\TaskBan_Testes.sqlite');
end;

{Inicialização de cada teste}
procedure TSuiteTarefa.Setup;
begin
  oTarefa:= TTarefa.Create;
end;

{Finalização de cada teste}
procedure TSuiteTarefa.TearDown;
begin
  oTarefa.Free;
end;

{CRUD - Create: Insere um registro na tabela}
procedure TSuiteTarefa.CRTarefaCreate(const PiIDCategoria, PiIDResponsavel, PiIDPara: Integer);
begin
  Assert.IsNotNull(oTarefa.TarefaDS);
  oTarefa.TarefaDS.ParamByName('idtarefa').AsInteger := -1;
  oTarefa.TarefaDS.Open();
  Assert.IsTrue(oTarefa.TarefaDS.Active);
  oTarefa.TarefaDS.Insert;
  Assert.IsTrue(oTarefa.TarefaDS.State = dsInsert);

  oTarefa.titulo := Format('Tarefa teste %d %d %d', [PiIDCategoria, PiIDResponsavel, PiIDPara]);
  oTarefa.idcategoriatarefa := PiIDCategoria;
  oTarefa.idcontato_responsavel := PiIDResponsavel;
  oTarefa.idcontato_para := PiIDPara;


  Assert.IsTrue(oTarefa.Save());

  Assert.AreEqual(oTarefa.idcategoriatarefa, PiIDCategoria);
  Assert.AreEqual(oTarefa.idcontato_responsavel, PiIDResponsavel);
  Assert.AreEqual(oTarefa.idcontato_para, PiIDPara);
  Assert.AreEqual(oTarefa.TarefaDS.FieldByName('tarefastatus').AsString, 'P');
  Assert.AreEqual(oTarefa.TarefaDS.FieldByName('idcategoriatarefa').AsInteger, PiIDCategoria);
  Assert.AreEqual(oTarefa.TarefaDS.FieldByName('idcontato_responsavel').AsInteger, PiIDResponsavel);
  Assert.AreEqual(oTarefa.TarefaDS.FieldByName('idcontato_pessoa').AsInteger, PiIDPara);

end;

{CRUD - Read: Lê um registro da tabela}
procedure TSuiteTarefa.CRTarefaRead(const PiIDTarefa: Integer);
begin
  Assert.IsNotNull(oTarefa.Load(PiIDTarefa));
  Assert.IsNotNull(oTarefa.TarefaDS);
  Assert.IsTrue(oTarefa.TarefaDS.Active);
  Assert.IsFalse(oTarefa.TarefaDS.IsEmpty);
  Assert.IsFalse(oTarefa.TarefaDS.Eof and oTarefa.TarefaDS.Bof);
  Assert.IsTrue(oTarefa.TarefaDS.RecordCount > 0);
  Assert.IsTrue(oTarefa.ID = PiIDTarefa);
  Assert.IsNotEmpty(oTarefa.titulo);
end;

{CRUD - Update: Altera um valor e grava na tabela}
procedure TSuiteTarefa.CRTarefaUpdate(const PiIDTarefa: Integer);
begin
  Assert.IsNotNull(oTarefa.Load(PiIDTarefa));
  oTarefa.TarefaDS.Edit;
  Assert.IsTrue(oTarefa.TarefaDS.State = dsEdit);
  oTarefa.titulo := oTarefa.titulo + ' ALTERADA';
  Assert.IsTrue(oTarefa.Save());
end;

{CRUD - Delete: Exclui um registro da tabela}
procedure TSuiteTarefa.CRTarefaDelete(const PiIDTarefa: Integer);
begin
  Assert.IsNotNull(oTarefa.Load(PiIDTarefa));
  Assert.IsTrue(oTarefa.Delete());
end;

{Caso de Teste: Inclui um comentário na tarefa}
procedure TSuiteTarefa.TestAddComentario(const PiIDContato: Integer; PsComentario: WideString);
begin
  Assert.IsTrue(oTarefa.AddComentario(PiIDContato, PsComentario));
  // ToDo: validar registro na oTarefa.TarefaHistoricoDS
end;

procedure TSuiteTarefa.TestAddHistoricoAlteracao(const PiIDContato: Integer; PsCampoAlterado: string;
      PsValorAntigo, PsValorNovo: WideString);
begin
  Assert.IsTrue(oTarefa.AddHistorico('H', PiIDContato, PsCampoAlterado, PsValorAntigo, PsValorNovo));
end;

{Caso de Teste: Carrega o histórico da tarefa}
procedure TSuiteTarefa.TestCarregaHistorico(PcTipo: String = ''; PiIDContato: integer = 0);
begin
  Assert.IsNotNull(oTarefa.LoadHistorico(PcTipo, PiIDContato));
  Assert.IsNotNull(oTarefa.TarefaHistoricoDS);
  Assert.IsTrue(oTarefa.TarefaHistoricoDS.Active);
  Assert.IsFalse(oTarefa.TarefaHistoricoDS.IsEmpty);
  Assert.IsFalse(oTarefa.TarefaHistoricoDS.Eof and oTarefa.TarefaHistoricoDS.Bof);
  Assert.IsTrue(oTarefa.TarefaHistoricoDS.RecordCount > 0);
  Assert.IsTrue(oTarefa.TarefaHistoricoDS.FieldByName('idtarefa').AsInteger = oTarefa.ID);
  Assert.IsNotEmpty(oTarefa.TarefaHistoricoDS.FieldByName('tarefahistoricovalornovo').AsString);
  if (PcTipo <> '') then
    Assert.AreEqual(oTarefa.TarefaHistoricoDS.FieldByName('tarefahistoricotipo').AsString, PcTipo);
  if (PiIDContato > 0) then
    Assert.AreEqual(oTarefa.TarefaHistoricoDS.FieldByName('idcontato').AsInteger, PiIDContato);
end;

{Caso de Teste: Altera Situação da tarefa}
procedure TSuiteTarefa.TestAlteraSituacao(PsNovaSituacao: string);
begin
  Assert.AreNotEqual(oTarefa.status, PsNovaSituacao);
  oTarefa.status := PsNovaSituacao;
  Assert.AreEqual(oTarefa.status, PsNovaSituacao);
end;


initialization
  TDUnitX.RegisterTestFixture(TSuiteTarefa);
end.
