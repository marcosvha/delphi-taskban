{-----------------------------------------------------------------------------
 Unit Name: MainForm
 Author:    Marcos VHA
 Date:      2017
 Purpose:   Form principal, que permite incluir e consultar tarefas.
            A ideia principal � o kanban de tarefas, que permite visualizar o fluxo.
            ToDo: Implementar pesquisa/filtros e melhorar usabilidade.
-----------------------------------------------------------------------------}
unit MainForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, ZAbstractRODataset,
  ZAbstractDataset, ZDataset, ZAbstractConnection, ZConnection, ZAbstractTable,
  Vcl.StdCtrls, Vcl.DBCtrls, Vcl.ExtCtrls, Vcl.DBCGrids, Vcl.ComCtrls,
  Vcl.Grids, Vcl.DBGrids;

type
  TfrmMain = class(TForm)
    qryToDo: TZQuery;
    dtsToDo: TDataSource;
    qryToDoidtarefa: TLargeintField;
    qryToDotarefastatus: TWideStringField;
    qryToDotarefanome: TWideStringField;
    qryToDotarefadesc: TBlobField;
    qryToDotarefadatacriacao: TDateTimeField;
    qryToDotarefatempoestimado: TFloatField;
    qryToDotarefatempodedicado: TFloatField;
    qryToDotarefatemporestante: TFloatField;
    qryToDotarefadatatermino: TDateTimeField;
    qryToDoidcategoriatarefa: TLargeintField;
    qryToDoidcontato_responsavel: TLargeintField;
    qryToDoidcontato_pessoa: TLargeintField;
    qryToDotarefadatavencimento: TDateTimeField;
    qryToDocategoriatarefanome: TStringField;
    qryDoing: TZQuery;
    LargeintField1: TLargeintField;
    WideStringField1: TWideStringField;
    WideStringField2: TWideStringField;
    BlobField1: TBlobField;
    DateTimeField1: TDateTimeField;
    FloatField1: TFloatField;
    FloatField2: TFloatField;
    FloatField3: TFloatField;
    DateTimeField2: TDateTimeField;
    DateTimeField3: TDateTimeField;
    LargeintField2: TLargeintField;
    LargeintField3: TLargeintField;
    LargeintField4: TLargeintField;
    StringField1: TStringField;
    dtsDoing: TDataSource;
    qryToDoresponsavelnome: TStringField;
    qryToDopessoanome: TStringField;
    qryDoingresponsavelnome: TStringField;
    qryDoingpessoanome: TStringField;
    qryDone: TZQuery;
    LargeintField5: TLargeintField;
    WideStringField3: TWideStringField;
    WideStringField4: TWideStringField;
    BlobField2: TBlobField;
    DateTimeField4: TDateTimeField;
    FloatField4: TFloatField;
    FloatField5: TFloatField;
    FloatField6: TFloatField;
    DateTimeField5: TDateTimeField;
    DateTimeField6: TDateTimeField;
    LargeintField6: TLargeintField;
    LargeintField7: TLargeintField;
    LargeintField8: TLargeintField;
    StringField2: TStringField;
    StringField3: TStringField;
    StringField4: TStringField;
    dtsDone: TDataSource;
    pcTarefas: TPageControl;
    tabKanban: TTabSheet;
    tabGrid: TTabSheet;
    Label1: TLabel;
    ctgToDo: TDBCtrlGrid;
    DBText1: TDBText;
    DBText2: TDBText;
    Label4: TLabel;
    DBText3: TDBText;
    Label5: TLabel;
    DBText4: TDBText;
    Label2: TLabel;
    ctgDoing: TDBCtrlGrid;
    DBText5: TDBText;
    DBText7: TDBText;
    Label7: TLabel;
    DBText8: TDBText;
    Label6: TLabel;
    DBText6: TDBText;
    Label3: TLabel;
    ctgDone: TDBCtrlGrid;
    DBText9: TDBText;
    DBText11: TDBText;
    Label10: TLabel;
    DBText12: TDBText;
    Label8: TLabel;
    DBText13: TDBText;
    qryTarefas: TZQuery;
    LargeintField9: TLargeintField;
    WideStringField5: TWideStringField;
    WideStringField6: TWideStringField;
    BlobField3: TBlobField;
    DateTimeField7: TDateTimeField;
    FloatField7: TFloatField;
    FloatField8: TFloatField;
    FloatField9: TFloatField;
    DateTimeField8: TDateTimeField;
    DateTimeField9: TDateTimeField;
    LargeintField10: TLargeintField;
    StringField5: TStringField;
    LargeintField11: TLargeintField;
    LargeintField12: TLargeintField;
    StringField6: TStringField;
    StringField7: TStringField;
    dtsTarefas: TDataSource;
    grdTarefas: TDBGrid;
    lblToDoCount: TLabel;
    lblDoingCount: TLabel;
    lblDoneCount: TLabel;
    qryTarefaHistorico: TZQuery;
    dtsTarefaHistorico: TDataSource;
    qryTarefaHistoricoidtarefa: TLargeintField;
    qryTarefaHistoricoidcontato: TLargeintField;
    qryTarefaHistoricotarefahistoricodataalteracao: TDateTimeField;
    qryTarefaHistoricotarefahistoricocampoalterado: TWideStringField;
    qryTarefaHistoricotarefahistoricovalorantigo: TWideStringField;
    qryTarefaHistoricotarefahistoricovalornovo: TWideStringField;
    qryTarefaHistoricotarefahistoricotipo: TWideStringField;
    qryTarefaComentarios: TZQuery;
    LargeintField13: TLargeintField;
    LargeintField14: TLargeintField;
    DateTimeField10: TDateTimeField;
    WideStringField7: TWideStringField;
    WideStringField8: TWideStringField;
    WideStringField9: TWideStringField;
    WideStringField10: TWideStringField;
    dtsTarefaComentarios: TDataSource;
    qryTarefaHistoricocontatonome: TStringField;
    qryTarefaComentarioscontatonome: TStringField;
    DBGrid2: TDBGrid;
    DBGrid3: TDBGrid;
    Label11: TLabel;
    Label13: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    btnAddTarefa: TButton;
    Label15: TLabel;
    DBText14: TDBText;
    Label16: TLabel;
    DBText15: TDBText;
    Label17: TLabel;
    DBText16: TDBText;
    procedure FormCreate(Sender: TObject);
    procedure ctgToDoMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ctgDoingDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ctgDoingDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure ctgToDoDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure ctgDoneDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure qryDoingAfterOpen(DataSet: TDataSet);
    procedure qryDoneAfterOpen(DataSet: TDataSet);
    procedure qryToDoAfterOpen(DataSet: TDataSet);
    procedure btnAddTarefaClick(Sender: TObject);
    procedure ctgToDoDblClick(Sender: TObject);
    procedure grdTarefasDblClick(Sender: TObject);
  private
    { Private declarations }
    procedure AtualizaKanban(DataSet: TDataSet);
    procedure AtualizaContadorTarefas(PoCtrlGrid: TDBCtrlGrid; PoDataSet: TDataSet; PoLabelCount: TLabel);
    procedure SetDropTaskStatus(DragSource: TObject; PsNovoStatus: string);
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

uses TaskBanDM, classTarefa, TarefaForm;

// --- M�TODOS DE USU�RIO ----------

{**
 * Ap�s alterar o valor, grava o registro para trocar de coluna no kanban
 * e atualiza os grids.
 *}
procedure TfrmMain.AtualizaKanban(DataSet: TDataSet);
begin
  if DataSet.State in [dsInsert, dsEdit] then
    DataSet.Post;
  qryToDo.Refresh;
  qryDoing.Refresh;
  qryDone.Refresh;
  qryTarefas.Refresh;
end;

{**
 * Atualiza labels com a quantidade de itens de cada situa��o.
 *}
procedure TfrmMain.AtualizaContadorTarefas(PoCtrlGrid: TDBCtrlGrid; PoDataSet: TDataSet; PoLabelCount: TLabel);
begin
  PoCtrlGrid.RowCount := PoDataSet.RecordCount;
  PoLabelCount.Caption := Format('%d tarefa(s)', [PoDataSet.RecordCount]);
end;

{**
 * Atribui o novo status ao registro correspondente ao objeto sendo dropado
 * e atualiza os grids.
 *}
procedure TfrmMain.SetDropTaskStatus(DragSource: TObject; PsNovoStatus: string);
var
  dsAux: TDataSet;
  oTarefa: TTarefa;
begin
  dsAux := TDBCtrlGrid(DragSource).DataSource.DataSet;
  oTarefa:= TTarefa.Create;
  try
    oTarefa.Load(dsAux.FieldByName('idtarefa').AsInteger);
    oTarefa.status := PsNovoStatus;
    oTarefa.Save();
  finally
    oTarefa.Free;
  end;
  AtualizaKanban(dsAux);
end;

// --- FIM DOS M�TODOS DE USU�RIO ----------


{**
 * Inicializa a aplica��o.
 *}
procedure TfrmMain.FormCreate(Sender: TObject);
begin
  dmTaskBan.ConectaBD();
  qryToDo.Open();
  qryDoing.Open();
  qryDone.Open();
  qryTarefas.Open();
  qryTarefaHistorico.Open();
  qryTarefaComentarios.Open();
  pcTarefas.ActivePageIndex := 0;

  // Workaround para ajustar altura dos PostIts
  ctgToDo.PanelHeight := 90;
  ctgDoing.PanelHeight := 90;
  ctgDone.PanelHeight := 90;
end;

procedure TfrmMain.qryToDoAfterOpen(DataSet: TDataSet);
begin
  AtualizaContadorTarefas(ctgToDo, qryToDo, lblToDoCount);
end;

procedure TfrmMain.qryDoingAfterOpen(DataSet: TDataSet);
begin
  AtualizaContadorTarefas(ctgDoing, qryDoing, lblDoingCount);
end;

procedure TfrmMain.qryDoneAfterOpen(DataSet: TDataSet);
begin
  AtualizaContadorTarefas(ctgDone, qryDone, lblDoneCount);
end;

{**
 * Altera��o de tarefa, � partir de duplo click no grid.
 *}
procedure TfrmMain.grdTarefasDblClick(Sender: TObject);
var
  dsAux: TDataSet;
begin
  dsAux := TDBGrid(Sender).DataSource.DataSet;
  if frmTarefa.OpenGUI( dsAux.FieldByName('idtarefa').AsInteger ) then
    AtualizaKanban(dsAux);
end;

{**
 * Altera��o de tarefa, � partir de duplo click no post-it.
 *}
procedure TfrmMain.ctgToDoDblClick(Sender: TObject);
var
  dsAux: TDataSet;
begin
  dsAux := TDBCtrlGrid(Sender).DataSource.DataSet;
  if frmTarefa.OpenGUI( dsAux.FieldByName('idtarefa').AsInteger ) then
    AtualizaKanban(dsAux);

  // Workaround: est� permanecendo em drag mode ap�s duplo clique
  //TDBCtrlGrid(Sender).EndDrag(false);
  Keybd_Event( VK_ESCAPE, MapVirtualKey( VK_ESCAPE, 0 ), 0, 0);
end;

procedure TfrmMain.ctgToDoDragDrop(Sender, Source: TObject; X, Y: Integer);
begin
  SetDropTaskStatus(Source, 'P'); // Pendente
end;

procedure TfrmMain.ctgToDoMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  TDBCtrlGrid(Sender).BeginDrag(False);
end;

procedure TfrmMain.ctgDoingDragDrop(Sender, Source: TObject; X, Y: Integer);
begin
  SetDropTaskStatus(Source, 'A'); // Andamento
end;

procedure TfrmMain.ctgDoingDragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
  Accept := Source is TDBCtrlGrid;
end;

procedure TfrmMain.ctgDoneDragDrop(Sender, Source: TObject; X, Y: Integer);
begin
  SetDropTaskStatus(Source, 'C'); // Conclu�do
end;

{**
 * Inclus�o de tarefa.
 *}
procedure TfrmMain.btnAddTarefaClick(Sender: TObject);
begin
  if frmTarefa.OpenGUI(0) then
    AtualizaKanban(qryTarefas);
end;

end.
