program TaskBan;

uses
  Vcl.Forms,
  MainForm in 'MainForm.pas' {frmMain},
  TaskBanDM in 'TaskBanDM.pas' {dmTaskBan: TDataModule},
  classTarefa in 'classTarefa.pas',
  Vcl.Themes,
  Vcl.Styles,
  TarefaForm in 'TarefaForm.pas' {frmTarefa};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'TaskBan';
  Application.CreateForm(TdmTaskBan, dmTaskBan);
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
