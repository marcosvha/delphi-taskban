object dmTaskBan: TdmTaskBan
  OldCreateOrder = False
  OnDestroy = DataModuleDestroy
  Height = 362
  Width = 707
  object conTaskBan: TZConnection
    ControlsCodePage = cCP_UTF16
    ClientCodepage = 'UTF-8'
    Catalog = ''
    Properties.Strings = (
      'codepage=UTF-8'
      'controls_cp=CP_UTF16')
    HostName = ''
    Port = 0
    Database = 'Database\TaskBanDB.sqlite'
    User = ''
    Password = ''
    Protocol = 'sqlite-3'
    LibraryLocation = 'sqlite3.dll'
    Left = 48
    Top = 32
  end
  object sqlProcCreateDB: TZSQLProcessor
    Params = <>
    Script.Strings = (
      
        '-- Creator:       MySQL Workbench 6.3.9/ExportSQLite Plugin 0.1.' +
        '0'
      '-- Author:        mhenke'
      '-- Caption:       New Model'
      '-- Project:       Name of the project'
      '-- Changed:       2017-03-31 23:09'
      '-- Created:       2017-03-31 17:58'
      'PRAGMA foreign_keys = OFF;'
      ''
      '-- Schema: TaskBan'
      'ATTACH "TaskBan.sdb" AS "TaskBan";'
      'BEGIN;'
      'CREATE TABLE "categoriatarefa"('
      
        '  "idcategoriatarefa" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL' +
        ' CHECK("idcategoriatarefa">=0),'
      '  "categoriatarefanome" VARCHAR(100)'
      ');'
      
        'INSERT INTO "categoriatarefa"("idcategoriatarefa","categoriatare' +
        'fanome") VALUES(1, '#39'ADMINISTRATIVA'#39');'
      
        'INSERT INTO "categoriatarefa"("idcategoriatarefa","categoriatare' +
        'fanome") VALUES(2, '#39'PROJETO'#39');'
      
        'INSERT INTO "categoriatarefa"("idcategoriatarefa","categoriatare' +
        'fanome") VALUES(3, '#39'APOIO'#39');'
      'CREATE TABLE "contato"('
      
        '  "idcontato" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL CHECK("' +
        'idcontato">=0),'
      '  "contatonome" VARCHAR(255) NOT NULL'
      ');'
      
        'CREATE INDEX "contato.contatonome_IDX" ON "contato" ("contatonom' +
        'e");'
      
        'INSERT INTO "contato"("idcontato","contatonome") VALUES(1, '#39'MARC' +
        'OS'#39');'
      
        'INSERT INTO "contato"("idcontato","contatonome") VALUES(2, '#39'DERP' +
        #39');'
      
        'INSERT INTO "contato"("idcontato","contatonome") VALUES(3, '#39'HERP' +
        #39');'
      
        'INSERT INTO "contato"("idcontato","contatonome") VALUES(4, '#39'DERP' +
        'INA'#39');'
      'CREATE TABLE "tarefa"('
      
        '  "idtarefa" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL CHECK("i' +
        'dtarefa">=0),'
      '  "tarefastatus" CHAR(1),'
      '  "tarefanome" VARCHAR(255) NOT NULL,'
      '  "tarefadesc" BLOB,'
      '  "tarefadatacriacao" DATETIME NOT NULL,'
      '  "tarefatempoestimado" FLOAT,'
      '  "tarefatempodedicado" FLOAT,'
      '  "tarefatemporestante" FLOAT,'
      '  "tarefadatavencimento" DATETIME,'
      '  "tarefadatatermino" DATETIME,'
      
        '  "idcategoriatarefa" INTEGER NOT NULL CHECK("idcategoriatarefa"' +
        '>=0),'
      
        '  "idcontato_responsavel" INTEGER NOT NULL CHECK("idcontato_resp' +
        'onsavel">=0),'
      
        '  "idcontato_pessoa" INTEGER NOT NULL CHECK("idcontato_pessoa">=' +
        '0),'
      '  CONSTRAINT "idtarefa_UNIQUE"'
      '    UNIQUE("idtarefa"),'
      '  CONSTRAINT "fk_tarefa_categoriatarefa1"'
      '    FOREIGN KEY("idcategoriatarefa")'
      '    REFERENCES "categoriatarefa"("idcategoriatarefa"),'
      '  CONSTRAINT "fk_tarefa_contato1"'
      '    FOREIGN KEY("idcontato_responsavel")'
      '    REFERENCES "contato"("idcontato"),'
      '  CONSTRAINT "fk_tarefa_contato2"'
      '    FOREIGN KEY("idcontato_pessoa")'
      '    REFERENCES "contato"("idcontato")'
      ');'
      'CREATE INDEX "tarefa.tarefanome_IDX" ON "tarefa" ("tarefanome");'
      
        'CREATE INDEX "tarefa.tarefadatacriacao_IDX" ON "tarefa" ("tarefa' +
        'datacriacao");'
      
        'CREATE INDEX "tarefa.tarefadatatermino_IDX" ON "tarefa" ("tarefa' +
        'datatermino");'
      
        'CREATE INDEX "tarefa.fk_tarefa_categoriatarefa1_idx" ON "tarefa"' +
        ' ("idcategoriatarefa");'
      
        'CREATE INDEX "tarefa.fk_tarefa_contato1_idx" ON "tarefa" ("idcon' +
        'tato_responsavel");'
      
        'CREATE INDEX "tarefa.fk_tarefa_contato2_idx" ON "tarefa" ("idcon' +
        'tato_pessoa");'
      'CREATE TABLE "tarefahistorico"('
      '  "idtarefa" INTEGER CHECK("idtarefa">=0),'
      '  "idcontato" INTEGER CHECK("idcontato">=0),'
      '  "tarefahistoricodataalteracao" DATETIME NOT NULL,'
      '  "tarefahistoricocampoalterado" VARCHAR(100) NOT NULL,'
      '  "tarefahistoricovalorantigo" VARCHAR(255) NOT NULL,'
      '  "tarefahistoricovalornovo" VARCHAR(255) NOT NULL,'
      '  "tarefahistoricotipo" CHAR(1) NOT NULL DEFAULT '#39'H'#39','
      '--   H=Hist'#243'rico de altera'#231#227'o'
      '--   C=Comentario'
      '  CONSTRAINT "fk_tarefahistorico_tarefa"'
      '    FOREIGN KEY("idtarefa")'
      '    REFERENCES "tarefa"("idtarefa"),'
      '  CONSTRAINT "fk_tarefahistorico_contato1"'
      '    FOREIGN KEY("idcontato")'
      '    REFERENCES "contato"("idcontato")'
      ');'
      
        'CREATE INDEX "tarefahistorico.fk_tarefahistorico_tarefa_idx" ON ' +
        '"tarefahistorico" ("idtarefa");'
      
        'CREATE INDEX "tarefahistorico.fk_tarefahistorico_contato1_idx" O' +
        'N "tarefahistorico" ("idcontato");'
      'COMMIT;')
    Connection = conTaskBan
    Delimiter = ';'
    Left = 176
    Top = 32
  end
  object qryContato: TZQuery
    Connection = conTaskBan
    SQL.Strings = (
      'select * from contato')
    Params = <>
    Left = 48
    Top = 112
  end
  object dtsContato: TDataSource
    DataSet = qryContato
    Left = 48
    Top = 161
  end
  object qryCategoriaTarefa: TZQuery
    Connection = conTaskBan
    SQL.Strings = (
      'select * from categoriatarefa')
    Params = <>
    Left = 219
    Top = 112
  end
  object dtsCategoriaTarefa: TDataSource
    DataSet = qryCategoriaTarefa
    Left = 219
    Top = 161
  end
  object qryContatoPara: TZQuery
    Connection = conTaskBan
    SQL.Strings = (
      'select * from contato')
    Params = <>
    Left = 128
    Top = 112
  end
  object dtsContatoPara: TDataSource
    DataSet = qryContatoPara
    Left = 128
    Top = 161
  end
end
