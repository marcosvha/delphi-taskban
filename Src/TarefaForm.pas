{-----------------------------------------------------------------------------
 Unit Name: TarefaForm
 Author:    Marcos VHA
 Date:      2017
 Purpose:   Formul�rio para manuten��o de uma Tarefa.
            Permite adicionar coment�rios e consultar hist�rico de altera��es.
-----------------------------------------------------------------------------}
unit TarefaForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.ComCtrls,
  classTarefa, Vcl.DBCtrls, DB;

type
  TfrmTarefa = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox3: TGroupBox;
    GroupBox2: TGroupBox;
    btnOK: TButton;
    btnCancelar: TButton;
    edtTitulo: TLabeledEdit;
    memDesc: TMemo;
    Descri��o: TLabel;
    edtTempoEstimado: TLabeledEdit;
    edtTempoDedicado: TLabeledEdit;
    edtTempoRestante: TLabeledEdit;
    edtDataCriacao: TDateTimePicker;
    Label1: TLabel;
    edtHoraCriacao: TDateTimePicker;
    Label2: TLabel;
    edtDataVencimento: TDateTimePicker;
    edtHoraVencimento: TDateTimePicker;
    Label3: TLabel;
    edtDataTermino: TDateTimePicker;
    edtHoraTermino: TDateTimePicker;
    rdgSituacao: TRadioGroup;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    edtIDTarefa: TLabeledEdit;
    memAddComentario: TMemo;
    btnAddComentario: TButton;
    cbxCategoria: TDBLookupComboBox;
    cbxResponsavel: TDBLookupComboBox;
    cbxPara: TDBLookupComboBox;
    memHistorico: TMemo;
    procedure FormDestroy(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtDataCriacaoChange(Sender: TObject);
    procedure edtDataVencimentoEnter(Sender: TObject);
    procedure edtDataTerminoEnter(Sender: TObject);
    procedure btnAddComentarioClick(Sender: TObject);
  private
    { Private declarations }
    procedure PreencheCampos();
    procedure CarregaHistorico();
    function SalvaTarefa(): boolean;
  public
    { Public declarations }
    FTarefa: TTarefa;
    function OpenGUI(PiIDTarefa: integer): boolean;
  end;

var
  frmTarefa: TfrmTarefa;

implementation

{$R *.dfm}

uses TaskBanDM;

// --- M�TODOS DE USU�RIO ----------

{**
 * M�todo para abrir o form e inicializar o objeto FTarefa, conforme o ID informado.
 * Se ID <= 0, � inclus�o de tarefa. Se maior, � altera��o.
 * Retorna True se for clicado no bot�o OK, e tarefa salva.
 * False se clicado em Cancelar ou fechar [X].
 *}
function TfrmTarefa.OpenGUI(PiIDTarefa: integer): boolean;
begin
  frmTarefa := TfrmTarefa.Create(Self);
  try
    frmTarefa.FTarefa:= TTarefa.Create;
    if PiIDTarefa > 0 then
      frmTarefa.FTarefa.Load(PiIDTarefa);
    Result:= frmTarefa.ShowModal() = mrOK;
  finally
    frmTarefa.Free;
  end;
end;

{**
 * Preenche os campos dos form com os valores do objeto FTarefa.
 * Se inclus�o, os valores default ser�o preenchidos.
 * Se altera��o, os valores carregados do BD no objeto � partir do ID.
 *}
procedure TfrmTarefa.PreencheCampos();
begin
  if FTarefa.ID > 0 then
    edtIDTarefa.Text := IntToStr(FTarefa.ID)
  else
    edtIDTarefa.Text := '(nova)';
  edtTitulo.Text := FTarefa.titulo;
  cbxCategoria.KeyValue := FTarefa.idcategoriatarefa;
  cbxResponsavel.KeyValue := FTarefa.idcontato_responsavel;
  cbxPara.KeyValue := FTarefa.idcontato_para;
  memDesc.Lines.Text := FTarefa.desc;
  edtDataCriacao.DateTime := FTarefa.datacriacao;
  edtHoraCriacao.DateTime := FTarefa.datacriacao;
  edtDataVencimento.DateTime := FTarefa.datavencimento;
  if (FTarefa.datavencimento > 0) then
    edtDataVencimento.Format := '';
  edtHoraVencimento.DateTime := FTarefa.datavencimento;
  edtDataTermino.DateTime := FTarefa.datatermino;
  if (FTarefa.datavencimento > 0) then
    edtDataTermino.Format := '';
  edtHoraTermino.DateTime := FTarefa.datatermino;

  edtTempoEstimado.Text := FloatToStr(FTarefa.tempoestimado);
  edtTempoDedicado.Text := FloatToStr(FTarefa.tempodedicado);
  edtTempoRestante.Text := FloatToStr(FTarefa.temporestante);

  case Ord(FTarefa.status[1]) of
    Ord('P'): rdgSituacao.ItemIndex := 0;
    Ord('A'): rdgSituacao.ItemIndex := 1;
    Ord('C'): rdgSituacao.ItemIndex := 2;
  end;

  CarregaHistorico();
end;

{**
 * Persiste os dados dos campos no BD, utilizando o objeto FTarefa.
 *}
function TfrmTarefa.SalvaTarefa(): boolean;
begin
  FTarefa.titulo := edtTitulo.Text;
  FTarefa.idcategoriatarefa := cbxCategoria.KeyValue;
  FTarefa.idcontato_responsavel := cbxResponsavel.KeyValue;
  FTarefa.idcontato_para := cbxPara.KeyValue;
  FTarefa.desc := memDesc.Lines.Text;
  FTarefa.datacriacao := edtDataCriacao.Date;// + edtHoraCriacao.Time;
  FTarefa.datavencimento := edtDataVencimento.Date; // + edtHoraVencimento.Time;
  FTarefa.datatermino := edtDataTermino.Date; // + edtHoraTermino.Time;

  FTarefa.tempoestimado := StrToFloat(edtTempoEstimado.Text);
  FTarefa.tempodedicado := StrToFloat(edtTempoDedicado.Text);
  FTarefa.temporestante := StrToFloat(edtTempoRestante.Text);

  FTarefa.status := C_STATUS_ARRAY[rdgSituacao.ItemIndex + 1];
  Result := FTarefa.Save();
end;

{**
 * Carrega o hist�rico de altera��es e coment�rio da tarefa.
 *}
procedure TfrmTarefa.CarregaHistorico();
var
  dsAux: TDataSet;
begin
  memHistorico.Lines.BeginUpdate;
  memHistorico.Lines.Clear();

  dsAux:= FTarefa.LoadHistorico();
  while not dsAux.EOF do
  begin
    if dsAux.FieldByName('tarefahistoricotipo').AsString = 'C' then
      memHistorico.Lines.Add(Format('%s %d comentou: %s', [
        FormatDateTime('dd/mm/yyyy HH:nn:ss', dsAux.FieldByName('tarefahistoricodataalteracao').AsDateTime),
        dsAux.FieldByName('idcontato').AsInteger, // ToDo: trazer o nome do usu�rio que efetuou a altera��o
        dsAux.FieldByName('tarefahistoricovalornovo').AsString
      ]))
    else
      memHistorico.Lines.Add(Format('Tarefa alterada: %s %d Campo=%s Valor anterior=%s Novo valor= %s', [
        FormatDateTime('dd/mm/yyyy HH:nn:ss', dsAux.FieldByName('tarefahistoricodataalteracao').AsDateTime),
        dsAux.FieldByName('idcontato').AsInteger, // ToDo: trazer o nome do usu�rio que efetuou a altera��o
        dsAux.FieldByName('tarefahistoricocampoalterado').AsString,
        dsAux.FieldByName('tarefahistoricovalorantigo').AsString,
        dsAux.FieldByName('tarefahistoricovalornovo').AsString
      ]));

    memHistorico.Lines.Add(' ');
    memHistorico.Lines.Add('--------------------------------------------------------');
    memHistorico.Lines.Add(' ');

    dsAux.Next();
  end;
  memHistorico.Lines.EndUpdate;
end;

// --- FIM DOS M�TODOS DE USU�RIO ----------

{**
 * Adiciona o conte�do digitado em memAddComentario como um coment�rio da tarefa.
 *}
procedure TfrmTarefa.btnAddComentarioClick(Sender: TObject);
begin
  FTarefa.AddComentario(cbxResponsavel.KeyValue, memAddComentario.Lines.Text);
  CarregaHistorico();
  memAddComentario.Lines.Clear();
end;

procedure TfrmTarefa.btnOKClick(Sender: TObject);
begin
  if SalvaTarefa() then
    Self.ModalResult := mrOk;
end;

{Workaround para o formato de data}
procedure TfrmTarefa.edtDataCriacaoChange(Sender: TObject);
begin
  if (TDateTimePicker(Sender).DateTime > 0) then
    TDateTimePicker(Sender).Format := '';
end;

{Workaround para o formato de data}
procedure TfrmTarefa.edtDataTerminoEnter(Sender: TObject);
begin
  if (TDateTimePicker(Sender).Date = 0) then
    TDateTimePicker(Sender).Date := now;
end;

{Workaround para o formato de data}
procedure TfrmTarefa.edtDataVencimentoEnter(Sender: TObject);
begin
  if (TDateTimePicker(Sender).Date = 0) then
    TDateTimePicker(Sender).Date := Now + 1;
end;

procedure TfrmTarefa.FormDestroy(Sender: TObject);
begin
  if Assigned(FTarefa) then
  begin
    FTarefa.TarefaDS.Close();
    FTarefa.TarefaHistoricoDS.Close();
    FTarefa.Free;
  end;
  frmTarefa := nil;
end;

procedure TfrmTarefa.FormShow(Sender: TObject);
begin
  PreencheCampos();
end;

end.
